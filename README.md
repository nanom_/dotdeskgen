# 🐧 Dotdeskgen, add your Linux apps to the panel
Dotdeskgen is a simple desktop entry generator built with PyQt5. it lets you
easily add app launchers to your DE panel, and also lets you manage them and
delete them.

# Installation
1. Download the precompiled binary from the
[releases page](https://gitlab.com/nanom_/dotdeskgen/-/releases)
or from
[here](https://cloud.operationtulip.com/s/wA3LYZFCPTwyD8X/download/dotdeskgen)
2. Run the application from your terminal with `./dotdeskgen`
3. If you can't run it, give execute permissions to the file with
`chmod +x dotdeskgen`
4. Done!

# Build it yourself
1. Install python3 and the following packages:
`pip3 install pyinstaller ini-parser`
2. Clone the repo `git clone https://gitlab.com/nanom_/dotdeskgen/`
3. Run `pyinstaller --onefile src/dotdeskgen.py`
4. The binary will be generated in dist/dotdeskgen but it won't work yet!
You need to include the UI files.
5. Open the generated dotdeskgen.spec file and add the following lines after
the `a = Analysis(...)` block:
```
a.datas += [('./ui/MainWindow.ui', './ui/MainWindow.ui', 'DATA'),
            ('./ui/AboutWindow.ui', './ui/AboutWindow.ui', 'DATA')]
```
6. Run `pyinstaller --onefile dotdeskgen.spec`
7. The file dist/dotdeskgen will be overwritten, this time it should work just
fine.

# CI/CD
coming soon

# .deb package
also coming soon (**maybe**)

# Other
(I think I am complying with the PyQt5 license, if not, please let me know.)
Dotdeskgen is free software released under the terms of the GNU GPL v3.
