import os
import sys
import ini
from PyQt5 import uic
from PyQt5.QtWidgets import \
    QMainWindow, \
    QApplication, \
    QFileDialog, \
    QMessageBox, \
    QDialog, \
    QDialogButtonBox, \
    QVBoxLayout, \
    QLabel

class AboutWindow(QDialog):
    def __init__(self, parent = None):
        super(AboutWindow, self).__init__(parent)
        uic.loadUi(resource_path("./ui/AboutWindow.ui"), self)
        self.btnClose.clicked.connect(self.btnCloseClicked)

    def btnCloseClicked(self):
        self.close()

class ConfirmationDialog(QDialog):
    def __init__(self, text, parent = None):
        super().__init__(parent)
        self.setWindowTitle("Please, confirm")

        btns = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        self.buttonBox = QDialogButtonBox(btns)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.layout = QVBoxLayout()
        message = QLabel(text)
        self.layout.addWidget(message)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)

class DotDeskGen(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        uic.loadUi(resource_path("./ui/MainWindow.ui"), self)

        self.refreshDesktopEntryList()

        self.btnBrowseBin.clicked.connect(self.btnBrowseBinClicked)
        self.btnBrowseIcon.clicked.connect(self.btnBrowseIconClicked)
        self.btnAddEntry.clicked.connect(self.btnAddEntryClicked)
        self.btnDeleteEntry.clicked.connect(self.btnDeleteEntryClicked)
        self.btnSaveEntryAs.clicked.connect(self.btnSaveEntryAsClicked)
        self.btnAbout.clicked.connect(self.btnAboutClicked)
        self.desktopEntryList.itemSelectionChanged.connect(
            self.desktopEntryListSelectionChanged
        )

    DesktopFileTemplate = \
"""[Desktop Entry]
Name={app_name}
Exec={app_bin}
Comment={app_description}
Terminal={is_terminal_app}
Icon={app_icon}
Type=Application
Categories={app_categories}
Keywords={app_keywords}
"""

    def showErrorDialog(self, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setText("Error")
        msg.setInformativeText(text)
        msg.setWindowTitle("Error")
        msg.exec_()

    def showInformationDialog(self, text):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("Information")
        msg.setInformativeText(text)
        msg.setWindowTitle("Information")
        msg.exec_()

    def resetForm(self):
        self.inputAppName.setText("")
        self.inputAppDescription.setText("")
        self.inputAppCategories.setText("")
        self.inputPathToBin.setText("")
        self.inputPathToIcon.setText("")
        self.cbIsTerminalApp.setChecked(False)

    def btnBrowseBinClicked(self):
        self.inputPathToBin.setText(QFileDialog.getOpenFileName(
            self, "Select binary file", os.getenv("HOME")
        )[0])

    def btnBrowseIconClicked(self):
        self.inputPathToIcon.setText(QFileDialog.getOpenFileName(
            self,
            "Select application icon",
            os.getenv("HOME"),
            "Images (*.png *.xpm *.jpg *.svg);;All Files (*)"
        )[0])

    def btnAddEntryClicked(self):
        home = os.getenv("HOME")
        entry_list_file_path = home + "/.config/dotdeskgen-entries"
        desktop_entries_path = home + "/.local/share/applications/"
        desktop_entry = None
        entries_file = None
        entry_already_exists = False

        app_name = self.inputAppName.text()
        app_description = self.inputAppDescription.text()
        app_categories = self.inputAppCategories.text()
        app_bin_path = self.inputPathToBin.text()
        app_icon_path = self.inputPathToIcon.text()
        is_terminal_app = str(self.cbIsTerminalApp.isChecked()).lower()
        entry_fname = app_name.lower().replace(" ", "-") + ".desktop"

        if app_name.strip() == "":
            self.showErrorDialog("Please enter the app name.")
            return
        if app_bin_path.strip() == "":
            self.showErrorDialog("Please enter the path of the app binary.")
            return
        if app_icon_path.strip() == "":
            self.showErrorDialog("Please enter the path of the app icon.")
            return

        existing_entries = [
            self.desktopEntryList.item(x).text() for x in range(
                self.desktopEntryList.count()
            )
        ]
        if entry_fname in existing_entries:
            entry_already_exists = True
            confirmation_dlg = ConfirmationDialog(
                "The entry " + entry_fname + " already exists. " + \
                "Do you want to update it?",
                self
            )
            if not confirmation_dlg.exec_():
                return

        try:
            desktop_entry = open(desktop_entries_path + entry_fname, "w")

            file_info = {
                "app_name": app_name,
                "app_bin": app_bin_path,
                "app_description": app_description,
                "is_terminal_app": is_terminal_app,
                "app_icon": app_icon_path,
                "app_categories": app_categories,
                "app_keywords": app_name.lower() + ";",
            }
            desktop_entry.write(self.DesktopFileTemplate.format(**file_info))
        except:
            self.showErrorDialog("Couldn't create the entry.")
            return
        finally:
            if not desktop_entry is None:
                desktop_entry.close()

        if not entry_already_exists:
            try:
                entries_file = open(entry_list_file_path, "a+")
                entries_file.write(entry_fname + "\n")
            except:
                self.showErrorDialog("Couldn't register the desktop entry.")
                return
            finally:
                if not entries_file is None:
                    entries_file.close()

        self.resetForm()
        if not entry_already_exists:
            self.showInformationDialog("Desktop entry successfully created!")
        else:
            self.showInformationDialog("Desktop entry updated!")
        self.refreshDesktopEntryList()

    def btnDeleteEntryClicked(self):
        home = os.getenv("HOME")
        entry_list_file_path = home + "/.config/dotdeskgen-entries"
        desktop_entries_path = home + "/.local/share/applications/"
        entries_file = None

        selected_items_list = self.desktopEntryList.selectedItems()
        selected_item = ""
        if len(selected_items_list) > 0:
            selected_item = selected_items_list[0].text()
        if selected_item.strip() == "":
            return

        confirmation_dlg = ConfirmationDialog(
            "Are you sure you want to delete " + selected_item + "?",
            self
        )
        if not confirmation_dlg.exec_():
            return

        try:
            os.remove(desktop_entries_path + selected_item)
        except:
            self.showErrorDialog(
                "Couldn't delete " + desktop_entries_path + selected_item
            )
            return

        try:
            entries_file = open(entry_list_file_path, "r+")

            lines = entries_file.readlines()
            entries_file.seek(0)
            for line in lines:
                if line[:len(line) - 1] != selected_item:
                    entries_file.write(line)
            entries_file.truncate()
        except:
            self.showErrorDialog(
                "Couldn't delete the entry from the list"
            )
            return
        finally:
            if not entries_file is None:
                entries_file.close()

        self.refreshDesktopEntryList()

    def btnSaveEntryAsClicked(self):
        app_name = self.inputAppName.text()
        app_description = self.inputAppDescription.text()
        app_categories = self.inputAppCategories.text()
        app_bin_path = self.inputPathToBin.text()
        app_icon_path = self.inputPathToIcon.text()
        is_terminal_app = str(self.cbIsTerminalApp.isChecked()).lower()

        if app_name.strip() == "":
            self.showErrorDialog("Please enter the app name.")
            return
        if app_bin_path.strip() == "":
            self.showErrorDialog("Please enter the path of the app binary.")
            return
        if app_icon_path.strip() == "":
            self.showErrorDialog("Please enter the path of the app icon.")
            return

        file = None
        filename = QFileDialog.getSaveFileName(
            self,
            "Save desktop entry as...",
            app_name.lower().replace(" ", "-") + ".desktop",
            "Desktop Entries (*.desktop);;All Files (*)"
        )
        if filename[0].strip() == "":
            return

        try:
            file = open(filename[0], "w")
            file_info = {
                "app_name": app_name,
                "app_bin": app_bin_path,
                "app_description": app_description,
                "is_terminal_app": is_terminal_app,
                "app_icon": app_icon_path,
                "app_categories": app_categories,
                "app_keywords": app_name.lower() + ";",
            }
            file.write(self.DesktopFileTemplate.format(**file_info))
            self.showInformationDialog("Desktop entry saved!")
        except:
            if file is None:
                self.showErrorDialog("Couldn't save the entry.")
        finally:
            if not file is None:
                file.close()

    def btnAboutClicked(self):
        about_window = AboutWindow(self)
        about_window.exec_()

    def desktopEntryListSelectionChanged(self):
        home = os.getenv("HOME")
        desktop_entries_path = home + "/.local/share/applications/"
        entry_file = None
        entry_info = {}

        selected_items_list = self.desktopEntryList.selectedItems()
        selected_item = ""
        if len(selected_items_list) > 0:
            selected_item = selected_items_list[0].text()
        if selected_item.strip() == "":
            return

        try:
            entry_file = open(desktop_entries_path + selected_item, "r")
            entry_info = ini.parse(entry_file.read().replace(";", ","))

            de = "Desktop Entry"
            self.resetForm()
            self.inputAppName.setText(entry_info[de]["Name"])
            self.inputAppDescription.setText(entry_info[de]["Comment"])
            self.inputAppCategories.setText(
                entry_info[de]["Categories"].replace(",", ";")
            )
            self.inputPathToBin.setText(entry_info[de]["Exec"])
            self.inputPathToIcon.setText(entry_info[de]["Icon"])
            self.cbIsTerminalApp.setChecked(entry_info[de]["Terminal"])
        except:
            self.showErrorDialog(
                "The desktop entry doesn't exist anymore or " + \
                "lacks important information"
            )
        finally:
            if not entry_file is None:
                entry_file.close()


    def refreshDesktopEntryList(self):
        home = os.getenv("HOME")
        entries_file = home + "/.config/dotdeskgen-entries"
        file = None
        self.desktopEntryList.clear()
        try:
            file = open(entries_file, "r")
            lines = file.readlines()
            for line in lines:
                self.desktopEntryList.addItem(line[:len(line) - 1])
        except FileNotFoundError:
            pass
        except:
            self.showErrorDialog("Couldn't open " + entries_file)
        finally:
            if not file is None:
                file.close()

# this is required by pyinstaller
def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath('.'), relative_path)

if __name__ == "__main__":
    app = QApplication([])
    window = DotDeskGen()
    window.show()
    sys.exit(app.exec_())
